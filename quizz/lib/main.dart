import 'package:flutter/material.dart';
import 'package:quizz/views/quizzHomePage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Questions/Réponses",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const QuizzHomePage(title: "Questions/Réponses"),
    );
  }
}
