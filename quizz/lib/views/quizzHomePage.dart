import 'package:flutter/material.dart';
import 'package:quizz/views/quizzPage.dart';


class QuizzHomePage extends StatefulWidget {
  const QuizzHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<QuizzHomePage> createState() => SomeQuizzHomePageState();
}


class SomeQuizzHomePageState extends State<QuizzHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text("Questions/Réponses"
            ,style: TextStyle(color: Colors.white, fontSize: 25)
          ),
          centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
        width: MediaQuery.of(context).size.width,
        decoration: new BoxDecoration(color: Colors.blueGrey),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            Container (
              width: 400.0,
              height: 150.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white24),
                borderRadius: BorderRadius.all(Radius.circular(10)),

              ),
              child: Text(
                "Si vous répondez une bonne réponse vous aurez +20 points et si vous "
                  "répondez une mauvaise réponse vous aurez -20 points et le résultat final vous sera communiqué à la fin",
                style: TextStyle(color: Colors.white, fontSize: 20),
                textAlign: TextAlign.center,
              ),),


            SizedBox(
              height: 25,
            ),

            GestureDetector(
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => QuizzPage()));
              },

              child: Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 54),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Text(
                  "Jouer à ce Quizz",
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}