import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:quizz/models/questionRepository.dart';
import 'package:quizz/models/question.dart';
import 'package:quizz/views/quizzFinalPage.dart';

class QuizzPage extends StatefulWidget {
  @override
  _QuizzPageState createState() => _QuizzPageState();
}

class _QuizzPageState extends State<QuizzPage>
    with SingleTickerProviderStateMixin {
  List<Question> questions = [];
  int index = 0;
  int points = 0;
  int correct = 0;
  int incorrect = 0;
  int notattempted = 0;

  late AnimationController controller;

  late Animation animation;

  double beginAnim = 0.0;

  double endAnim = 1.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    questions = getQuestions();

    controller =
        AnimationController(duration: const Duration(seconds: 5), vsync: this);
    animation = Tween(begin: beginAnim, end: endAnim).animate(controller)
      ..addListener(() {
        setState(() {
          // Change here any Animation object value.
        });
      });

    startProgress();

    animation.addStatusListener((AnimationStatus animationStatus) {
      if (animationStatus == AnimationStatus.completed) {
        if (index < questions.length - 1) {
          index++;
          resetProgress();
          startProgress();
        } else {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => Result(
                    score: points,
                    totalQuestion: questions.length,
                    correct: correct,
                    incorrect: incorrect,
                    notattempted: notattempted,
                  )));
        }
      }
    });
  }

  startProgress() {
    controller.forward();
  }

  stopProgress() {
    controller.stop();
  }

  resetProgress() {
    controller.reset();
  }

  void nextQuestion() {
    if (index < questions.length - 1) {
      index++;
      resetProgress();
      startProgress();
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Result(
                score: points,
                totalQuestion: questions.length,
                correct: correct,
                incorrect: incorrect,
                notattempted: notattempted,
              )));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      backgroundColor: Colors.blueGrey,
      title: Text("Questions/Réponses"
          ,style: TextStyle(color: Colors.white, fontSize: 20)
      ),
      centerTitle: true,
    ),


      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        width: MediaQuery.of(context).size.width,
        decoration: new BoxDecoration(color: Colors.blueGrey),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Container(
                height: 4.0,
                child: LinearProgressIndicator(
                  value: animation.value,
                )),
            CachedNetworkImage(
              imageUrl: questions[index].getImageUrl(),
            ),
            SizedBox(
              height: 25,
            ),
            Container (
              width: 350.0,
              height: 80.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white24),
                borderRadius: BorderRadius.all(Radius.circular(10)),

              ),
              child : Text(
              questions[index].getQuestion() + "?",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w500),
            ),),

            Spacer(flex: 4),

            Container(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Expanded(
                      child: GestureDetector(
                        onTap: () {
                          if (questions[index].getAnswer() == "True") {
                            setState(() {
                              points = points + 20;
                              nextQuestion();
                              correct++;
                            });
                          } else {
                            setState(() {
                              points = points - 20;
                              nextQuestion();
                              incorrect++;
                            });
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(10)),
                          child: Text(
                            "VRAI",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )),
                  SizedBox(
                    width: 40,
                  ),
                  Expanded(
                      child: GestureDetector(
                        onTap: () {
                          if (questions[index].getAnswer() == "False") {
                            setState(() {
                              points = points + 20;
                              nextQuestion();
                              correct++;
                            });
                          } else {
                            setState(() {
                              points = points - 20;
                              nextQuestion();
                              incorrect++;
                            });
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(10)),
                          child: Text(
                            "FAUX",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }
}
